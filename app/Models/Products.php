<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;

    protected $table = 'products';
    protected $primaryKey = 'id';

    protected $fillable = [
        'articl_code',
        'barcode',
        'designation',
        'barcode',
        'description',
        'category',
        'brand',
        'price',
        'quantity',
        'measure',
        'stock_alert'
    ];

    public function Categories() {
        return $this->belongsTo('App\Models\Categories');
    }
}
