<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductsController;

Route::group([
    'prefix' => 'v1/auth',
    'as' => 'users'
], function () {
    Route::post('/login', 'AuthController@login');
    Route::post('/logout', 'AuthController@logout');
    Route::post('/register', 'AuthController@register');
    Route::post('me', 'AuthController@me');
    Route::post('send-password-reset-link', 'ResetPasswordController@sendEmail');
    Route::post('reset-password', 'ChangePasswordController@process');
    Route::get('/all-users', 'AuthController@getAllUsers');
});

Route::group([
    'prefix' => 'v1/products',
    'as' => 'products'
], function() {
    Route::get('/list', 'ProductsController@getAll');
    Route::get('/product/{id}', 'ProductsController@getById');
    Route::post('/create', 'ProductsController@create');
    Route::post('/update/{id}', 'ProductsController@update');
    Route::delete('/delete/{id}', 'ProductsController@destroy');
    Route::get('/search', 'ProductsController@search');
    Route::get('/nextAC', 'ProductsController@getNextArticleCode');
});

Route::group([
    'prefix' => 'v1/categories',
    'as' => 'categories'
], function() {
    Route::get('/list', 'CategoriesController@getAll');
    Route::get('/category/{id}', 'CategoriesController@getById');
    Route::post('/create', 'CategoriesController@create');
    Route::get('/{des}', 'CategoriesController@getIdOfDes');
    Route::post('/update/{id}', 'CategoriesController@update');
    Route::delete('/delete/{id}', 'CategoriesController@destroy');
    Route::get('/status/activated', 'CategoriesController@activatedCategories');
    Route::get('/status/deactivated', 'CategoriesController@deactivatedCategories');
    Route::post('/change/{id}', 'CategoriesController@changeStatus');
    // Route::get('/designation/{id}', 'CategoriesController@getDesignationById');
});

Route::group([
    'prefix' => 'v1/brands',
    'as' => 'brands'
], function() {
    Route::get('/list', 'BrandsController@getAll');
    Route::get('/brand/{id}', 'BrandsController@getById');
    Route::post('/create', 'BrandsController@create');
    Route::get('/{des}', 'BrandsController@getIdOfDes');
    Route::post('/update/{id}', 'BrandsController@update');
    Route::delete('/delete/{id}', 'BrandsController@destroy');
    Route::get('/status/activated', 'BrandsController@activatedBrands');
    Route::get('/status/deactivated', 'BrandsController@deactivatedBrands');
    Route::post('/change/{id}', 'BrandsController@changeStatus');
});
