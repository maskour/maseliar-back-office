<?php

namespace App\Http\Controllers;

use App\Models\Order__details;
use Illuminate\Http\Request;

class OrderDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order__details  $order__details
     * @return \Illuminate\Http\Response
     */
    public function show(Order__details $order__details)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order__details  $order__details
     * @return \Illuminate\Http\Response
     */
    public function edit(Order__details $order__details)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order__details  $order__details
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order__details $order__details)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order__details  $order__details
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order__details $order__details)
    {
        //
    }
}
