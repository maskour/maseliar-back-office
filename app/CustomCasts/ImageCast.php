<?php

namespace App\CustomCasts;

use Movor\LaravelCustomCasts\CustomCastBase;
use Illuminate\Http\UploadedFile;

class ImageCast extends CustomCastBase
{
    use Storage;

    public function setAttribute($file)
    {
        // Define storage folder
        // (relative to "storage/app" folder in Laravel project)
        // Don't forget to create it !!!
        $storageDir = 'images';

        // Generate random image name
        $filename = str_random() . '.' . $file->extension();

        // Save image to predefined folder
        $file->storeAs($storageDir, $filename);

        // This will be stored in db field: "image"
        return $storageDir . '/' . $filename;
    }

    public function deleted()
    {
        // We can access underlying model with $this->model
        // and attribute name that is being casted with $this->attribute

        // Retrieve image path and delete it from the disk
        $imagePath = $this->model->image;
        Storage::delete($imagePath);
    }

}
