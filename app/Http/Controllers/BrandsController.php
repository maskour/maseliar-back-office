<?php

namespace App\Http\Controllers;

use App\Models\Brands;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Enums\globalVars;

class BrandsController extends Controller
{
    public function create(Request $request): \Illuminate\Http\JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'designation' => 'required|unique:products|max:500',
        ]);
        if ($validator -> fails()) {
            Session::flash('error', $validator->messages());
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        $brand = Brands::where('designation', $request->designation)->first();

        if (!$brand) {
            $brand = new Brands();
            $brand->designation = $request->designation;
            if($request->hasFile('image')) {
                $file = $request->file('image');
                $fileName = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $picture   = date('His').'-'.$fileName;
                $destinationPath = public_path().'/images/';
                $file->move($destinationPath, $picture);
                $brand->image = $picture;
            }

            if ($request->has('status'))
                $brand->status = $request->status;
            else
                $brand->status = "Disponible";

            $brand->save();
            return response()->json(['Brand' => $brand, 'message' => 'Brand crée avec succès'], Response::HTTP_OK );
        }

        else
            return response()->json(['message' => 'Brand déja existe'], Response::HTTP_BAD_REQUEST );
    }

    public function getAll(): \Illuminate\Http\JsonResponse
    {
        $brands = Brands::orderBy('id', 'asc')->get();
        return response()->json(['Brands' => $brands, 'message' => 'Brands listés avec succès'], Response::HTTP_OK);
    }

    public function getIdOfDes($des): int
    {
        $brand = Brands::select('id')->where('designation', $des)->first();
//        if (is_null($brand)) {
//            return response()->json(['message' => 'Marque introuvable'], Response::HTTP_BAD_REQUEST );
//        }
        return $brand->id;
    }

    public function getById($id): \Illuminate\Http\JsonResponse
    {
        $brand = Brands::find($id);
        if (is_null($brand)) {
            return response()->json(['message' => 'Brand introuvable'], Response::HTTP_BAD_REQUEST );
        }
        return response()->json(['Brand' => $brand, 'message' => 'Brand est trouvé'], Response::HTTP_OK);
    }

    public function getDesById($id): string
    {
        $brand = Brands::find($id);
        return $brand->designattion;
    }

    public function isVerified($request, $this_Brand): bool
    {
        $brand = Brands::select('id')->where('designation', $request->designation)->first();
        if (!is_null($brand)) {
            if ($brand->id == $this_Brand->id)
                return true;
            return false;
        }
        return true;
    }

    public function update(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'designation' => 'nullable'
        ]);
        if ($validator -> fails()) {
            Session::flash('error', $validator->messages());
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        $brand = Brands::find($id);
        $data = $request->all();
        if (!is_null($brand)) {
            $fields = ['designation', 'status', 'image'];
            $counter = 0;
            foreach ($fields as $field) {
                if ($request->has($field))
                    $brand[$field] = $data[$field] != null ? $data[$field] : $brand[$field];
                else $counter += 1;
            }
            if ($counter == count($fields))
                return response()->json(["message" => "Tous les champs vides"], Response::HTTP_BAD_REQUEST);
            else {
                if (!$this->isVerified($request, $brand))
                    return response()->json(["message" => "Brand '$brand->designation' déja existe"], Response::HTTP_BAD_REQUEST);
                $brand->save();
                return response()->json(["Brand" => $brand, "message" => "Brand '$brand->designation' modifié avec succès"], Response::HTTP_OK);
            }
        }

        return response()->json(['message' => 'Brand introuvable'], Response::HTTP_BAD_REQUEST);
    }

    public function destroy($id): \Illuminate\Http\JsonResponse
    {
        $brand = Brands::find($id);
        if (is_null($brand)) {
            return response()->json(['message' => 'Brand introuvable'], Response::HTTP_BAD_REQUEST );
        }
        $brand->delete();
        return response()->json(["message" => "Brand '$brand->designation' supprimé avec succès"], Response::HTTP_OK);
    }

    public function activatedBrands(): \Illuminate\Http\JsonResponse
    {
        $brand = Brands::select('*')
            ->where("status", "Disponible")
            ->orderBy('created_at', 'desc')
            ->get();
        return response()->json(['Brands' => $brand, 'message' => 'Brands trouvés'], Response::HTTP_OK);
    }

    public function deactivatedBrands(): \Illuminate\Http\JsonResponse
    {
        $brand = Brands::select('*')
            ->where("status", "Indisponible")
            ->orderBy('created_at', 'desc')
            ->get();
        return response()->json(['Brands' => $brand, 'message' => 'Brands trouvés'], Response::HTTP_OK);
    }

    public function changeStatus($id): \Illuminate\Http\JsonResponse
    {
        $brand = Brands::find($id);
        if ($brand->status == "Disponible")
            $brand->status = "Indisponible";
        else
            $brand->status = "Disponible";

        $brand->save();
        return response()->json(['Brand' => $brand, 'message' => 'Status changé'], Response::HTTP_OK);
    }
}
