<?php

namespace App\Http\Controllers;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetPasswordMail;
use Carbon\Carbon;

class ResetPasswordController extends Controller
{
    public function sendEmail(Request $request): \Illuminate\Http\JsonResponse
    {
        if (!$this->validateEmail($request->email)) {
            return $this->failedResponse();
        }
        $this->send($request->email);
        return $this->successResponse();
    }

    public function send($email) {
        $token = $this->createToken($email);
        Mail::to($email)->send(new ResetPasswordMail($token));
    }

    public function createToken($email) {
        $oldToken = DB::table('password_resets')->where('email', $email)->first();
        $user = User::whereEmail($email)->first();
        if ($oldToken) {
            $user->update(['remember_token' => $oldToken->token]);
            return $oldToken->token;
        }

        $token = $this->str_random(60);
        $user->update(['remember_token' => $token]);
        DB::table('password_resets')->insert([
            'email' => $email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);
        return $token;
    }

    function str_random($length=10): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function validateEmail($email): bool
    {
        return !!User::where('email', $email)->first();
    }

    public function failedResponse(): \Illuminate\Http\JsonResponse
    {
        return response()->json(['message' => 'E-mail introuvable dans notre base de données.'], Response::HTTP_NOT_FOUND);
    }

    public function successResponse(): \Illuminate\Http\JsonResponse
    {
        return response()->json(['message' => 'E-mail de réinitialisation est envoyé avec succès, s’il vous plaît vérifier votre boîte de réception.'], Response::HTTP_OK);
    }
}
