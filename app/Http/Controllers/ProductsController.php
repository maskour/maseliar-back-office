<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\BrandsController;
use App\Models\Products;
use Illuminate\Http\Request;
use App\Enums\eResponseCode;
use App\Enums\globalVars;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use App\Libs\Tools;

class ProductsController extends Controller
{

    public function create(Request $request): \Illuminate\Http\JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'designation' => 'required|unique:products|max:500',
            'description' => 'nullable',
            'category' => 'required',
            'brand' => 'nullable',
            's_price' => 'required|numeric',
            'stock' => 'required|numeric',
            'measure' => 'nullable',
        ]);
        if ($validator -> fails()) {
            Session::flash('error', $validator->messages());
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        $tool = new Tools();
        //$data = $request->all();
        $product = Products::where('designation', $request->designation)->first();

        if (!$product) {
            $product = new Products();
            $categoryController = new CategoriesController();
            $brandController = new BrandsController();
            $category_id = $categoryController->getIdOfDes($request->category);
            $brand_id = $brandController->getIdOfDes($request->brand);
            $current_date = new \DateTime();
            $next_id = $tool->getLastInsertedId("products");
            $id_with_zeros = substr(str_repeat(0, 3).$next_id, - 3);
            $product->barcode = $request->barcode;
            $product->article_code = $current_date->format('ymd') . '-' . $id_with_zeros;
            $product->designation = $request->designation;
            $product->description = $request->description;
            $product->category_id = intval($category_id);
            $product->brand_id = intval($brand_id);
            $product->s_price = $request->s_price;
            if ($request->p_price)
                $product->p_price = $request->p_price;
            else
                $product->p_price = 0;
            $product->stock = $request->stock;
            $product->measure = $request->measure;
            if ($request->has('stock_alert'))
                $product->stock_alert = $request->stock_alert;
            else
                $product->stock_alert = globalVars::STOCK_ALERT;

            $product->save();
            return response()->json(['Product' => $product, 'message' => 'Produit crée avec succès'], Response::HTTP_OK );
        }

        return response()->json(['message' => 'Produit déja existe'], Response::HTTP_BAD_REQUEST );
    }

    public function getAll(): \Illuminate\Http\JsonResponse
    {
        $products = Products::orderBy('created_at', 'asc')->get();
        //        $categoryController = new CategoriesController();
        //        $category = $categoryController->getDesById($products->id);
        return response()->json(['Products' => $products, 'message' => 'Produits listés avec succès'], Response::HTTP_OK);
    }

    public function getById($id): \Illuminate\Http\JsonResponse
    {
        $product = Products::find($id);
        if (is_null($product)) {
            return response()->json(['message' => 'Produit introuvable'], Response::HTTP_BAD_REQUEST );
        }
        return response()->json(['Products' =>$product, 'message' => 'Produit est trouvé'], Response::HTTP_OK);
    }

    public function getByArticleCode($article_code): \Illuminate\Http\JsonResponse
    {
        $product = Products::find($article_code);
        if (is_null($product)) {
            return response()->json(['message' => 'Produit introuvable'], Response::HTTP_BAD_REQUEST );
        }
        return response()->json(['Products' => $product, 'message' => 'Produit est trouvé'], Response::HTTP_OK);
    }

    public function getNextArticleCode(): \Illuminate\Http\JsonResponse
    {
        $tool = new Tools();
        //return $tool->nextArticleCode("products");
        return response()->json(['next_article_code' => $tool->nextArticleCode("products")], Response::HTTP_OK);
    }

    public function isVerified($request, $this_product): bool
    {
        $product = Products::select('id')->where('designation', $request->designation)->first();
        if (!is_null($product)) {
            if ($product->id == $this_product->id)
                return true;
            return false;
        }
        return true;
    }

    public function update(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'designation' => 'nullable',
            'barcode' => 'nullable',
            'description' => 'nullable',
            'category_id' => 'nullable',
            'brand_id' => 'nullable',
            's_price' => 'numeric',
            'p_price' => 'numeric',
            'stock' => 'numeric',
            'measure' => 'nullable',
            'stock_alert' => 'nullable'
        ]);
        if ($validator -> fails()) {
            Session::flash('error', $validator->messages());
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        $product = Products::find($id);
        $data = $request->all();
        if (!is_null($product)) {
            $categoryController = new CategoriesController();
            $brandController = new BrandsController();
            $category_id = $categoryController->getIdOfDes($request->category);
            $brand_id = $brandController->getIdOfDes($request->brand);
            $fields = ['designation', 'description', 'category_id', 'brand_id', 'p_price', 's_price', 'stock', 'measure', 'stock_alert'];
            $counter = 0;
            foreach ($fields as $field) {
                if ($request->has($field)) {
                    if ($field == "category_id")
                        $product[$field] = $data["category_id"] != null ? $category_id : $product[$field];
                    else if ($field == "brand_id")
                        $product[$field] = $data["brand_id"] != null ? $brand_id : $product[$field];
                    else
                        $product[$field] = $data[$field] != null ? $data[$field] : $product[$field];
                }
                else $counter += 1;
            }
            if ($counter == count($fields))
                return response()->json(["message" => "Tous les champs vides"], Response::HTTP_BAD_REQUEST);
            else {
                if (!$this->isVerified($request, $product))
                    return response()->json(["message" => "Produit '$product->designation' déja existe"], Response::HTTP_BAD_REQUEST);
                $product->save();
                return response()->json(["Produit" => $product, "message" => "Produit '$product->designation' modifié avec succès"], Response::HTTP_OK);
            }
        }

        return response()->json(['message' => 'Produit introuvable'], Response::HTTP_BAD_REQUEST);
    }

    public function destroy($id): \Illuminate\Http\JsonResponse
    {
        $product = Products::find($id);
        if (is_null($product)) {
            return response()->json(['message' => 'Produit introuvable'], Response::HTTP_BAD_REQUEST );
        }
        $product->delete();
        return response()->json(["message" => "Produit '$product->designation' supprimé avec succès"], Response::HTTP_OK);
    }

    public function search($searchTerm): \Illuminate\Http\JsonResponse
    {
        $product = Products::query()
            ->where('designation', 'LIKE', "%{$searchTerm}%")
            ->orWhere('article_code', 'LIKE', "%{$searchTerm}%")
            ->get();

        if (is_null($product))
            return response()->json(['message' => 'Aucun produit trouvé avec ce élément'], Response::HTTP_BAD_REQUEST);
        return response()->json(['Products' => $product, 'message' => 'Produit est trouvé'], Response::HTTP_OK);
    }
}
