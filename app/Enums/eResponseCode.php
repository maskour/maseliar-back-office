<?php

namespace App\Enums;


abstract class eResponseCode
{

    /*
     * examples :
     * de 00 à 04 sont réservés pour les actions suivants, tout autre action nécessite un code supérieur à 04
     * const S_LISTED_200_00 = ['S200_00' => 'Sheets succèsfully listed']; 00 -> pour lister
     * const S_GET_200_01 = ['S200_01' => 'Sheet returned succèssfully'];  01 -> retourner un element
     * const S_GET_200_01x1 = ['S200_01x1' => 'Sheet returned succèssfully']; 01xN -> attention à utiliser uniquement si pour le même code on a des variantes, le même example peut être appliquer sur les autres codes
     * const S_CREATED_200_02 = ['S200_02' => 'Sheet succèsfully created'];02 -> créer element
     * const S_UPDATED_200_03 = ['S200_03' => 'Sheet succèsfully updated'];03 -> mettre à jour
     * const S_DELETED_200_04 = ['S200_04' => 'Sheet succèsfully deleted'];04 -> supprimer un element
     *
     */

    // Login
    const LOGGED_IN_INFO_200_01 = ['L200_01' => 'Vous êtes désormais connecté(e)'];
    const LOGGED_IN_200_05      = ['L200_05' => 'Vous êtes désormais connecté(e)'];
    const LOGGED_OUT_200_06     = ['L200_06' => 'Vous êtes désormais déconnecté(e)'];
    const LOGGED_401_01         = ['L401_01' => 'Mot de passe ou adresse email incorrects'];
    const LOGGED_401_02         = ['L401_02' => 'Invalid Token'];

    // Products
    const P_CREATE_200_02 = ['P200_02' => 'Product créé avec succès'];
    const P_UPDATE_200_03 = ['P200_03' => 'Product modifié avec succès'];
    const P_LISTED_200_00 = ['P200_00' => 'Products listés avec succès'];
    const P_ID_FOUND_200_00 = ['P200_00' => 'Product est trouvé'];

    const P_NOTEXISTS_400_01 = ['P400_01' => 'Product introuvable'];
    const P_ALREADY_CREATED_400_02 = ['F400_02' => 'Product déja existe'];
}
