<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('barcode')->nullable();
            $table->string('article_code')->unique();
            $table->string('designation')->unique();
            $table->text('description')->nullable();
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->double('p_price')->nullable();
            $table->double('s_price');
            $table->integer('stock');
            $table->string('measure')->nullable();
            $table->integer('stock_alert')->default(50);
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->engine = 'InnoDB';
        });
    }


    public function down()
    {
        Schema::dropIfExists('products');
    }
}
