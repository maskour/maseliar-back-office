<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    use HasFactory;

    protected $table = 'categories';
    protected $primaryKey = 'id';
    protected $fillable = [
        'designation',
        'image'
    ];

//    protected $casts = [
//        'image' => ImageCast::class
//    ];

    public function Products() {
        return $this->hasMany('App\Models\Products');
    }
}
