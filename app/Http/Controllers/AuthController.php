<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Validator;
use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{
    //private $apiToken;
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'getAllUsers']]);
        //$this->apiToken = uniqid(base64_encode(Str::random(60)));
    }

    public function register(Request $request): \Illuminate\Http\JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:4,100|unique:users',
            'email' => 'required|email|unique:users',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        $user = User::where('email', $request->email)->orWhere('name', $request->name)->first();
        if ($user)
        {
            $res['data'] = $user;
            $res['status'] = 0;
            $res['message'] = 'L\'utilisateur existe déjà';
            $res_code = Response::HTTP_CONFLICT;
        }
        else {
            $user = User::create(array_merge(
                $validator->validated(),
                ['password' => $request->password]
            ));
            $res['data'] = $user;
            $res['status'] = 1;
            $res['message'] = 'L\'utilisateur s\'est inscrit avec succès';
            $res_code = Response::HTTP_OK;
        }

        return response()->json($res, $res_code);
    }

    public function login(Request $request): \Illuminate\Http\JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'password' => 'required|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $token = auth()->attempt($validator->validated());
        if (!$token) {
            $res['status'] = 0;
            $res['message'] = 'Username ou mot de passe incorrect';
            return response()->json($res, Response::HTTP_UNAUTHORIZED);
        }

        $user = auth()->user();
        $token = auth()->claims(['user_id' => $user->id, 'name' => $user->name, 'email' => $user->email, 'created_at' => $user->created_at])->attempt($validator->validated());
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => $user,
            'status' => 1,
            'message' => 'Connecter avec succès'
        ]);
    }

    /**
     * Sign out
     */
    public function signout(): \Illuminate\Http\JsonResponse
    {
        auth()->logout();
        return response()->json(['message' => 'User loged out']);
    }

    /**
     * Token refresh
     */
    public function refresh(): \Illuminate\Http\JsonResponse
    {
        return $this->generateToken(auth()->refresh());
    }

    /**
     * User
     */
    public function user(): \Illuminate\Http\JsonResponse
    {
        return response()->json(auth()->user());
    }

    /**
     * Generate token
     */
    protected function generateToken($token): \Illuminate\Http\JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }

    public function guard()
    {
        return Auth::guard();
    }

    public function getAllUsers(): \Illuminate\Http\JsonResponse
    {
        $users = User::orderBy('name', 'asc')->get();
        //User::all();
        return response()->json(['users' => $users, 'message' => 'Successfully listed'], Response::HTTP_OK);
    }
}
