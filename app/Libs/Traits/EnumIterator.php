<?php

namespace App\Libs\Traits;

trait EnumIterator {

    static function getAll($class) {
        $Class = new \ReflectionClass($class);
        return $Class->getConstants();
    }
}
