<?php

namespace App\Libs;

use App\Models\Products;
use Illuminate\Support\Facades\DB;

class Tools
{
    public function getLastInsertedId($table): int
    {
        if ($table == 'products') {
            if (Products::count() > 0) {
                $product = Products::find(DB::table($table)->max('id'));
                return $product->id + 1;
            }
            else {
                return 1;
            }
        }

        return 0;
    }

    public function nextArticleCode($table): string {
        $next_article_code = '';
        $current_date = new \DateTime();
        if ($table == 'products') {
            $next_id = $this->getLastInsertedId($table);
            $id_with_zeros = substr(str_repeat(0, 3).$next_id, - 3);
            $next_article_code = $current_date->format('ymd') . '-' . $id_with_zeros;
        }

        return $next_article_code;
    }
}
