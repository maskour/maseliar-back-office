<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Enums\globalVars;
use phpDocumentor\Reflection\Types\Integer;

class CategoriesController extends Controller
{

    public function create(Request $request): \Illuminate\Http\JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'designation' => 'required|unique:products|max:500',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048|max:2048'
        ]);
        if ($validator -> fails()) {
            Session::flash('error', $validator->messages());
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        $category = Categories::where('designation', $request->designation)->first();

        if (!$category) {
            $category = new Categories();
            $category->designation = $request->designation;
            if($request->hasFile('image')) {
                $file = $request->file('image');
                $fileName = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $picture   = date('His').'-'.$fileName;
                $destinationPath = public_path().'/images/';
                $file->move($destinationPath, $picture);
                $category->image = $picture;
            }

            if ($request->has('status'))
                $category->status = $request->status;
            else
                $category->status = "Disponible";

            $category->save();
            return response()->json(['Category' => $category, 'message' => 'Category crée avec succès'], Response::HTTP_OK );
        }

        else
            return response()->json(['message' => 'Category déja existe'], Response::HTTP_BAD_REQUEST );
    }

    public function getAll(): \Illuminate\Http\JsonResponse
    {
        $categories = Categories::orderBy('id', 'asc')->get();
        return response()->json(['Categories' => $categories, 'message' => 'Categories listés avec succès'], Response::HTTP_OK);
    }

    public function getIdOfDes($des): int
    {
        $category = Categories::select('id')->where('designation', $des)->first();
//        if (is_null($category)) {
//            return response()->json(['message' => 'Category introuvable'], Response::HTTP_BAD_REQUEST );
//        }
        return $category->id;
    }

    public function getById($id): \Illuminate\Http\JsonResponse
    {
        $category = Categories::find($id);
        if (is_null($category)) {
            return response()->json(['message' => 'Category introuvable'], Response::HTTP_BAD_REQUEST );
        }
        return response()->json(['Category' => $category, 'message' => 'Category est trouvé'], Response::HTTP_OK);
    }

//    public function getDesignationById($id): \Illuminate\Http\JsonResponse
//    {
//        $category = Categories::find($id);
//        //return $category->designattion;
//        return response()->json(['category_designation' => $category], Response::HTTP_OK);
//    }

    public function getDesById($id): string
    {
        $category = Categories::find($id);
        return $category->designattion;
    }

    public function isVerified($request, $this_category): bool
    {
        $category = Categories::select('id')->where('designation', $request->designation)->first();
        if (!is_null($category)) {
            if ($category->id == $this_category->id)
                return true;
            return false;
        }
        return true;
    }

    public function update(Request $request, $id): \Illuminate\Http\JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'designation' => 'nullable'
        ]);
        if ($validator -> fails()) {
            Session::flash('error', $validator->messages());
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        $category = Categories::find($id);
        $data = $request->all();
        if (!is_null($category)) {
            $fields = ['designation', 'status', 'image'];
            $counter = 0;
            foreach ($fields as $field) {
                if ($request->has($field))
                    $category[$field] = $data[$field] != null ? $data[$field] : $category[$field];
                else $counter += 1;
            }
            if ($counter == count($fields))
                return response()->json(["message" => "Tous les champs vides"], Response::HTTP_BAD_REQUEST);
            else {
                if (!$this->isVerified($request, $category))
                    return response()->json(["message" => "Category '$category->designation' déja existe"], Response::HTTP_BAD_REQUEST);
                $category->save();
                return response()->json(["Category" => $category, "message" => "Category '$category->designation' modifié avec succès"], Response::HTTP_OK);
            }
        }

        return response()->json(['message' => 'Category introuvable'], Response::HTTP_BAD_REQUEST);
    }

    public function destroy($id): \Illuminate\Http\JsonResponse
    {
        $category = Categories::find($id);
        if (is_null($category)) {
            return response()->json(['message' => 'Category introuvable'], Response::HTTP_BAD_REQUEST );
        }
        $category->delete();
        return response()->json(["message" => "Category '$category->designation' supprimé avec succès"], Response::HTTP_OK);
    }

    public function activatedCategories(): \Illuminate\Http\JsonResponse
    {
        $category = Categories::select('*')
            ->where("status", "Disponible")
            ->orderBy('created_at', 'desc')
            ->get();
        return response()->json(['Categories' => $category, 'message' => 'Categories trouvés'], Response::HTTP_OK);
    }

    public function deactivatedCategories(): \Illuminate\Http\JsonResponse
    {
        $category = Categories::select('*')
            ->where("status", "Indisponible")
            ->orderBy('created_at', 'desc')
            ->get();
        return response()->json(['Categories' => $category, 'message' => 'Categories trouvés'], Response::HTTP_OK);
    }

    public function changeStatus($id): \Illuminate\Http\JsonResponse
    {
        $category = Categories::find($id);
        if ($category->status == "Disponible")
            $category->status = "Indisponible";
        else
            $category->status = "Disponible";

        $category->save();
        return response()->json(['Category' => $category, 'message' => 'Status changé'], Response::HTTP_OK);
    }
}
