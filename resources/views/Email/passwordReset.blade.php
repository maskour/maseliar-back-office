@component('mail::message')
# Changer le mot de passe

Vous recevez cet e-mail parce que vous avez demandé une réinitialisation du mot de passe de votre compte sur le POS.
Cliquez sur le bouton ci-dessous pour changer votre mot de passe:

@component("mail::button", ["url" => "http://localhost:4200/response-password-reset?token=".$token])
Réinitialiser le mot de passe
@endcomponent

Merci,<br>
{{ config('app.name') }}
@endcomponent
