<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Models\User;

class ChangePasswordController extends Controller
{
    public function process(ChangePasswordRequest $request): \Illuminate\Http\JsonResponse
    {
        return $this->getPasswordResetTableRow($request)->count() > 0 ? $this->changePassword($request) : $this->tokenNotFound();
    }

    private function getPasswordResetTableRow($request): \Illuminate\Database\Query\Builder
    {
        return DB::table('password_resets')->where('token', $request->resetToken);
    }

    private function tokenNotFound(): \Illuminate\Http\JsonResponse
    {
        return response()->json(['message' => 'Token ou email incorrect'], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    private function changePassword($request): \Illuminate\Http\JsonResponse
    {
        $user = User::where('remember_token', $request->resetToken)->first();
        $user->update(['password' => $request->password]);
        $this->getPasswordResetTableRow($request)->delete();
        return response()->json(['message' => 'Mot de passe changé avec succès'], Response::HTTP_CREATED);
    }
}
